<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>net.guerlab</groupId>
	<artifactId>guerlab-excel</artifactId>
	<version>1.3.0</version>

	<name>${project.groupId}:${project.artifactId}</name>
	<description>net.guerlab.excel is a suite of poi expanded libraries that include utility classes and much much more.</description>
	<url>https://gitee.com/guerlab_net/guerlab-excel</url>

	<licenses>
		<license>
			<name>GNU LESSER GENERAL PUBLIC LICENSE Version 3</name>
			<url>https://www.gnu.org/licenses/lgpl-3.0.txt</url>
		</license>
	</licenses>

	<developers>
		<developer>
			<id>guer</id>
			<name>guer</name>
			<email>master@guerlab.net</email>
			<organization>guerlab</organization>
			<organizationUrl>http://www.guerlab.net</organizationUrl>
		</developer>
	</developers>
    
    <organization>
        <name>guerlab</name>
        <url>http://www.guerlab.net</url>
    </organization>

	<scm>
		<connection>scm:git:https://gitee.com/guerlab_net/guerlab-excel.git</connection>
		<developerConnection>scm:git:https://gitee.com/guerlab_net/guerlab-excel.git</developerConnection>
		<url>https://gitee.com/guerlab_net/guerlab-excel</url>
		<tag>HEAD</tag>
	</scm>
    
    <issueManagement>
        <system>gitee</system>
        <url>https://gitee.com/guerlab_net/guerlab-excel/issues</url>
    </issueManagement>

	<properties>
		<java.version>1.8</java.version>
		<project.encoding>UTF-8</project.encoding>
		
		<maven-gpg-plugin.version>1.6</maven-gpg-plugin.version>
		
		<commons.version>1.3.0</commons.version>
		<poi.version>3.17</poi.version>
	</properties>

	<repositories>
		<repository>
			<id>guerlab</id>
			<url>http://maven.guerlab.net/repository/public/</url>
		</repository>
	</repositories>
	
	<pluginRepositories>
		<pluginRepository>
			<id>guerlab</id>
			<url>http://maven.guerlab.net/repository/public/</url>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<downloadUrl>https://gitee.com/guerlab_net</downloadUrl>
		<repository>
			<id>guerlab-release</id>
			<name>guerlab Release Repository</name>
			<url>http://maven.guerlab.net/repository/guerlab-release</url>
		</repository>
		<snapshotRepository>
			<id>guerlab-snapshot</id>
			<name>guerlab Snapshot Repository</name>
			<url>http://maven.guerlab.net/repository/guerlab-snapshot</url>
		</snapshotRepository>
	</distributionManagement>
	
	<profiles>
		<profile>
			<id>guerlab</id>
			<repositories>
				<repository>
					<id>guerlab-snapshot</id>
					<name>guerlab Snapshots</name>
					<url>http://maven.guerlab.net/repository/guerlab-snapshot</url>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</repository>
				<repository>
					<id>guerlab-releases</id>
					<name>guerlab Releases</name>
					<url>http://maven.guerlab.net/repository/release</url>
					<snapshots>
						<enabled>false</enabled>
					</snapshots>
				</repository>
			</repositories>
			<pluginRepositories>
				<pluginRepository>
					<id>guerlab-snapshot</id>
					<name>guerlab Snapshots</name>
					<url>http://maven.guerlab.net/repository/guerlab-snapshot</url>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</pluginRepository>
			</pluginRepositories>
		</profile>
		<profile>
			<id>central</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-gpg-plugin</artifactId>
						<version>${maven-gpg-plugin.version}</version>
						<executions>
							<execution>
								<id>sign-artifacts</id>
								<phase>verify</phase>
								<goals>
									<goal>sign</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
			<distributionManagement>
				<snapshotRepository>
					<id>sonatype-nexus-snapshots</id>
					<name>Sonatype Nexus Snapshots</name>
					<url>https://oss.sonatype.org/content/repositories/snapshots/</url>
				</snapshotRepository>
				<repository>
					<id>sonatype-nexus-staging</id>
					<name>Nexus Release Repository</name>
					<url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
				</repository>
			</distributionManagement>
		</profile>
	</profiles>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-failsafe-plugin</artifactId>
					<executions>
						<execution>
							<goals>
								<goal>integration-test</goal>
								<goal>verify</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>pl.project13.maven</groupId>
					<artifactId>git-commit-id-plugin</artifactId>
					<executions>
						<execution>
							<goals>
								<goal>revision</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<verbose>true</verbose>
						<dateFormat>yyyy-MM-dd'T'HH:mm:ssZ</dateFormat>
						<generateGitPropertiesFile>true</generateGitPropertiesFile>
						<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<configuration>
						<source>${java.version}</source>
						<target>${java.version}</target>
						<encoding>${project.encoding}</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<configuration>
						<encoding>${project.encoding}</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<configuration>
						<encoding>${project.encoding}</encoding>
						<aggregate>true</aggregate>
						<charset>${project.encoding}</charset>
						<docencoding>${project.encoding}</docencoding>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>net.guerlab</groupId>
				<artifactId>guerlab-commons</artifactId>
				<version>${commons.version}</version>
			</dependency>
			<dependency>
				<groupId>org.apache.poi</groupId>
				<artifactId>poi-ooxml</artifactId>
				<version>${poi.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<dependency>
			<groupId>net.guerlab</groupId>
			<artifactId>guerlab-commons</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
		</dependency>
	</dependencies>
</project>