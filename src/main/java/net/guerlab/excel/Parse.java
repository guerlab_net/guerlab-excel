package net.guerlab.excel;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.guerlab.commons.number.NumberHelper;
import net.guerlab.commons.time.TimeHelper;

/**
 * 解析对象
 *
 * @author guer
 *
 */
public abstract class Parse {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parse.class);

    /**
     * 单元格信息集合
     */
    protected final List<CellInfo> cellInfos = new ArrayList<>();

    /**
     * 开始行号
     */
    protected int start;

    /**
     * 设置开始行号
     *
     * @param start
     *            开始行号
     */
    final void setStart(
            int start) {
        this.start = start;
    }

    /**
     * 返回单元格信息集合
     *
     * @return 单元格信息集合
     */
    public final List<CellInfo> getCellInfos() {
        return cellInfos;
    }

    /**
     * 添加单元格信息
     *
     * @param cellInfo
     *            单元格信息
     */
    final void addCellInfo(
            CellInfo cellInfo) {
        if (cellInfo == null) {
            return;
        }

        cellInfos.add(cellInfo);
    }

    /**
     * 添加单元格信息集合
     *
     * @param cellInfoCollection
     *            单元格信息集合
     */
    final void addAllCellInfo(
            Collection<CellInfo> cellInfoCollection) {
        if (cellInfoCollection == null || cellInfoCollection.isEmpty()) {
            return;
        }

        cellInfos.addAll(cellInfoCollection.stream().filter(Objects::nonNull).collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Parse [\r\n");
        cellInfos.stream().forEach(e -> {
            builder.append("\t");
            builder.append(e);
            builder.append("\r\n");
        });
        builder.append("]");
        return builder.toString();
    }

    /**
     * 解析excel文件, 当解析失败返回Collections.emptyList()对象
     *
     * @param <T>
     *            实体类类型
     * @param filePath
     *            excel文件路径
     * @param clazz
     *            实体类类型对象
     * @return 对象列表
     */
    public final <T> List<T> parse(
            String filePath,
            Class<T> clazz) {
        return parse(new File(filePath), clazz);
    }

    /**
     * 解析excel文件, 当解析失败返回Collections.emptyList()对象
     *
     * @param <T>
     *            实体类类型
     * @param file
     *            excel文件
     * @param clazz
     *            实体类类型对象
     * @return 对象列表
     */
    public final <T> List<T> parse(
            File file,
            Class<T> clazz) {
        try (Workbook wb = ExcelHelp.getWorkbook(file)) {

            ArrayList<T> list = new ArrayList<>();

            Sheet sheet = wb.getSheetAt(0);

            int firstRowIndex = Math.max(start, sheet.getFirstRowNum());
            int lastRowIndex = sheet.getLastRowNum();

            for (int rIndex = firstRowIndex; rIndex <= lastRowIndex; rIndex++) {
                readRow(list, clazz, sheet, rIndex);
            }

            return list;
        } catch (Exception e) {
            LOGGER.trace(e.getMessage(), e);
            return Collections.emptyList();
        }
    }

    /**
     * 解析单行
     *
     * @param list
     *            对象列表
     * @param sheet
     *            工作表对象
     * @param rIndex
     *            行ID
     * @throws IllegalAccessException
     *             当类不可访问的时候抛出IllegalAccessException异常
     * @throws InstantiationException
     *             当类实例化失败的时候抛出InstantiationException异常
     */
    private final <T> void readRow(
            ArrayList<T> list,
            Class<T> clazz,
            Sheet sheet,
            int rIndex) throws InstantiationException, IllegalAccessException {
        Row row = sheet.getRow(rIndex);

        if (row == null) {
            return;
        }

        T obj = clazz.newInstance();

        for (CellInfo cellInfo : cellInfos) {
            readRowOnce(cellInfo, clazz, obj, row);
        }

        list.add(obj);
    }

    private static <T> void readRowOnce(
            CellInfo cellInfo,
            Class<T> clazz,
            T obj,
            Row row) {
        String value = getStringValue(cellInfo, row);
        Method method = getWriteMethod(cellInfo, clazz);

        if (value == null || method == null) {
            return;
        }

        try {
            write0(method, obj, value);
        } catch (Exception e) {
            LOGGER.trace(e.getMessage(), e);
        }
    }

    private static <T> void write0(
            Method method,
            T obj,
            String value) throws IllegalAccessException, InvocationTargetException {
        Class<?> fieldType = method.getParameterTypes()[0];

        if (writeNumber(method, obj, value, fieldType)) {
            return;
        }
        if (writeTime(method, obj, value, fieldType)) {
            return;
        }
        writeOther(method, obj, value, fieldType);
    }

    private static <T> boolean writeNumber(
            Method method,
            T obj,
            String value,
            Class<?> fieldType) throws IllegalAccessException, InvocationTargetException {
        if (fieldType == Long.class || fieldType == Long.TYPE) {
            method.invoke(obj, Long.valueOf(value));
        } else if (fieldType == Integer.class || fieldType == Integer.TYPE) {
            method.invoke(obj, Integer.valueOf(value));
        } else if (fieldType == Double.class || fieldType == Double.TYPE) {
            method.invoke(obj, Double.valueOf(value));
        } else if (fieldType == BigDecimal.class) {
            method.invoke(obj, new BigDecimal(value));
        } else {
            return false;
        }
        return true;
    }

    private static <T> boolean writeTime(
            Method method,
            T obj,
            String value,
            Class<?> fieldType) throws IllegalAccessException, InvocationTargetException {
        if (fieldType == Date.class) {
            method.invoke(obj, TimeHelper.parse(value));
        } else if (fieldType == LocalDateTime.class) {
            method.invoke(obj, TimeHelper.parseLocalDateTime(value));
        } else if (fieldType == LocalDate.class) {
            method.invoke(obj, TimeHelper.parseLocalDateTime(value).toLocalDate());
        } else if (fieldType == LocalTime.class) {
            method.invoke(obj, TimeHelper.parseLocalDateTime(value).toLocalTime());
        } else if (fieldType == Year.class) {
            method.invoke(obj, Year.parse(value));
        } else if (fieldType == Month.class) {
            method.invoke(obj, Month.of(Integer.parseInt(value)));
        } else {
            return false;
        }
        return true;
    }

    private static <T> void writeOther(
            Method method,
            T obj,
            String value,
            Class<?> fieldType) throws IllegalAccessException, InvocationTargetException {
        if (fieldType == Boolean.class || fieldType == Boolean.TYPE) {
            method.invoke(obj, Boolean.valueOf(value));
        } else if (fieldType.isEnum()) {
            method.invoke(obj, getEnumValue(fieldType, value));
        } else {
            method.invoke(obj, value);
        }
    }

    private static String getStringValue(
            CellInfo cellInfo,
            Row row) {
        Integer cellIndex = cellInfo.getCellIndex();

        String defaultValue = cellInfo.getDefaultValue();

        String value = null;

        if (NumberHelper.greaterOrEqualZero(cellIndex)) {
            value = ExcelHelp.getStringValue(row, cellIndex);
        }

        if (value == null && defaultValue != null) {
            value = defaultValue;
        }

        return value;
    }

    private static Method getWriteMethod(
            CellInfo cellInfo,
            Class<?> clazz) {
        try {
            return new PropertyDescriptor(cellInfo.getFieldName(), clazz).getWriteMethod();
        } catch (IntrospectionException e) {
            LOGGER.trace(e.getMessage(), e);
            return null;
        }
    }

    private static Enum<?> getEnumValue(
            Class<?> fieldType,
            String value) {
        for (Object p : fieldType.getEnumConstants()) {
            Enum<?> tempEnum = (Enum<?>) p;
            if (tempEnum != null && tempEnum.name().equals(value)) {
                return tempEnum;
            }
        }
        return null;
    }
}
